// ---------------------------------------------------------------------------
// This example shows how to use NewPing's ping_timer method which uses the Timer2 interrupt to get the
// ping time. The advantage of using this method over the standard ping method is that it permits a more
// event-driven sketch which allows you to appear to do two things at once. An example would be to ping
// an ultrasonic sensor for a possible collision while at the same time navigating. This allows a
// properly developed sketch to multitask. Be aware that because the ping_timer method uses Timer2,
// other features or libraries that also use Timer2 would be effected. For example, the PWM function on
// pins 3 & 11 on Arduino Uno (pins 9 and 11 on Arduino Mega) and the Tone library. Note, only the PWM
// functionality of the pins is lost (as they use Timer2 to do PWM), the pins are still available to use.
// NOTE: For Teensy/Leonardo (ATmega32U4) the library uses Timer4 instead of Timer2.
// ---------------------------------------------------------------------------
#include <Arduino.h>
#include "NewPing.h"
#include "Bluetooth_HC05.h"

#define TRIGGER_PIN_1   2 // Arduino pin tied to trigger pin on ping sensor.
#define ECHO_PIN_1      3 // Arduino pin tied to echo pin on ping sensor.

#define TRIGGER_PIN_2   4 // Arduino pin tied to trigger pin on ping sensor.
#define ECHO_PIN_2      5 // Arduino pin tied to echo pin on ping sensor.

#define TRIGGER_PIN_3   8 // Arduino pin tied to trigger pin on ping sensor.
#define ECHO_PIN_3      9 // Arduino pin tied to echo pin on ping sensor.

#define TRIGGER_PIN_4   6 // Arduino pin tied to trigger pin on ping sensor.
#define ECHO_PIN_4      7 // Arduino pin tied to echo pin on ping sensor.

#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

#define ULTRASONIC_NUM 4
#define PING_INTERVAL 33

void echoCheck();
void oneSensorCycle();

NewPing UltraSonic[ULTRASONIC_NUM] = {     // Sensor object array.
  NewPing(TRIGGER_PIN_1, ECHO_PIN_1, MAX_DISTANCE),
  NewPing(TRIGGER_PIN_2, ECHO_PIN_2, MAX_DISTANCE),
  NewPing(TRIGGER_PIN_3, ECHO_PIN_3, MAX_DISTANCE),
  NewPing(TRIGGER_PIN_4, ECHO_PIN_4, MAX_DISTANCE)
};

Bluetooth_HC05 hc05; // Bluetooth module object

unsigned long pingTimer[ULTRASONIC_NUM]; // Holds the times when the next ping should happen for each sensor.
unsigned int cm[ULTRASONIC_NUM];         // Where the ping distances are stored.
uint8_t currentSensor = 0;          // Keeps track of which sensor is active.

void setup() {
  // Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.

  /* Speed: 38400; HC-05 RESET: pin 2; HC-05 MODE (PIO11): pin 3 */
  hc05.begin(9600, 10, 11, HC05_MODE_DATA);
  /* Wait until HC-05 starts */
  delay(700);
  hc05.setRole(HC05_ROLE_SLAVE);
  /* Cannot connect without this */
  hc05.initSerialPortProfile();
  /* Slave module says "+ADDR:11:4:290255" on "AT+ADDR?" command */
  hc05.println("============================");
  pinMode(LED_BUILTIN, OUTPUT);

  pingTimer[0] = millis() + 75;           // First ping starts at 75ms, gives time for the Arduino to chill before starting.
  for (uint8_t i = 1; i < ULTRASONIC_NUM; i++) // Set the starting time for each sensor.
    pingTimer[i] = pingTimer[i - 1] + PING_INTERVAL;
}

void loop() {
  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) { // Loop through all the sensors.
      if (millis() >= pingTimer[i]) {         // Is it this sensor's time to ping?
        pingTimer[i] += PING_INTERVAL * ULTRASONIC_NUM;  // Set next time this sensor will be pinged.
        if (i == 0 && currentSensor == ULTRASONIC_NUM - 1) oneSensorCycle(); // Sensor ping cycle complete, do something with the results.
        UltraSonic[currentSensor].timer_stop();          // Make sure previous timer is canceled before starting a new ping (insurance).
        currentSensor = i;                          // Sensor being accessed.
        cm[currentSensor] = 0;                      // Make distance zero in case there's no ping echo for this sensor.
        UltraSonic[currentSensor].ping_timer(echoCheck); // Do the ping (processing continues, interrupt will call echoCheck to look for echo).
      }
    }
    // Other code that *DOESN'T* analyze ping results can go here.
}

void echoCheck() { // If ping received, set the sensor distance to array.
  if (UltraSonic[currentSensor].check_timer())
    cm[currentSensor] = UltraSonic[currentSensor].ping_result / US_ROUNDTRIP_CM;
}

void oneSensorCycle() { // Sensor ping cycle complete, do something with the results.
  // The following code would be replaced with your code that does something with the ping results.
  String payload;
  for (uint8_t i = 0; i < ULTRASONIC_NUM; i++){
    // payload += i;
    // payload += '=';
    payload += cm[i];
    if(i != ULTRASONIC_NUM-1) payload += ",";
  }
  hc05.println(payload);

  // for (uint8_t i = 0; i < ULTRASONIC_NUM; i++) {
  //   hc05.print("UltraSonic ");
  //   hc05.print(i+1);
  //   hc05.print("= ");
  //   hc05.print(cm[i]);
  //   hc05.print("cm ");
  //   hc05.println();

    // Serial.print("UltraSonic ");
    // Serial.print(i+1);
    // Serial.print("= ");
    // Serial.print(cm[i]);
    // Serial.print("cm ");
    // Serial.println();
  // }
  // hc05.println("=====");
  // Serial.println("=====");
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}
