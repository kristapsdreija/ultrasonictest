#! /usr/bin/python
import serial
from Tkinter import *
import Tkinter as tk
import sys
from time import sleep

bluetoothSerial = serial.Serial(port='COM19',baudrate=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)
print("Bluetooth connected")

def update_distance():
    try:
        data = bluetoothSerial.readline()
        if not data: return 0
        data = data.decode()
        # print data
    except:
        print "Error while readline()"

    # Update the message
    data = data.rstrip()
    data_split = data.split(',')
    distance_1["text"] = "Sensor 1: " +data_split[0] + " cm"
    distance_2["text"] = "Sensor 2: " +data_split[1] + " cm"
    distance_3["text"] = "Sensor 3: " +data_split[2] + " cm"
    distance_4["text"] = "Sensor 4: " +data_split[3] + " cm"

    if int(data_split[0]) <= 5 and int(data_split[0]) > 0:
        colour_1.set('gold')
    elif int(data_split[0]) <= 10 and int(data_split[0]) > 5:
        colour_1.set('lime green')
    elif int(data_split[0]) <= 25 and int(data_split[0]) > 10:
        colour_1.set('blue')
    else:
        colour_1.set('red')
    distance_1.configure(fg=colour_1.get())

    if int(data_split[1]) <= 5 and int(data_split[1]) > 0:
        colour_2.set('gold')
    elif int(data_split[1]) <= 10 and int(data_split[1]) > 0:
        colour_2.set('lime green')
    elif int(data_split[1]) <= 25 and int(data_split[1]) > 10:
        colour_2.set('blue')
    else:
        colour_2.set('red')
    distance_2.configure(fg=colour_2.get())

    if int(data_split[2]) <= 5 and int(data_split[2]) > 0:
        colour_3.set('gold')
    elif int(data_split[2]) <= 10 and int(data_split[2]) > 0:
        colour_3.set('lime green')
    elif int(data_split[2]) <= 25 and int(data_split[2]) > 10:
        colour_3.set('blue')
    else:
        colour_3.set('red')
    distance_3.configure(fg=colour_3.get())

    if int(data_split[3]) <= 5 and int(data_split[3]) > 0:
        colour_4.set('gold')
    elif int(data_split[3]) <= 10 and int(data_split[3]) > 0:
        colour_4.set('lime green')
    elif int(data_split[3]) <= 25 and int(data_split[3]) > 10:
        colour_4.set('blue')
    else:
        colour_4.set('red')
    distance_4.configure(fg=colour_4.get())

    # After 1 second, update the status
    root.after(50, update_distance)

if __name__ == '__main__':
    try:
        root = tk.Tk()

        colour_1 = StringVar()
        colour_2 = StringVar()
        colour_3 = StringVar()
        colour_4 = StringVar()

        colour_1.set('black')
        colour_2.set('black')
        colour_3.set('black')
        colour_4.set('black')

        distance_1 = tk.Label(root,fg = colour_1.get(), font=("Helvetica", 48))
        distance_1.grid()
        distance_2 = tk.Label(root, fg = colour_2.get(), font=("Helvetica", 48))
        distance_2.grid()
        distance_3 = tk.Label(root, fg = colour_3.get(), font=("Helvetica", 48))
        distance_3.grid()
        distance_4 = tk.Label(root, fg = colour_4.get(), font=("Helvetica", 48))
        distance_4.grid()

        root.after(1, update_distance)
        root.mainloop()

    except KeyboardInterrupt:
        bluetoothSerial.close()
        root.destroy()
    except:
        print "Random error"
