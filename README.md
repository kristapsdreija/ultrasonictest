# UltrasonicTest repository
# Kristaps Dreija & Shahab
# Tartu, 2016

## How to test the device?
The prototype works as follows:
1. The MCU pings all four sensors and calculates the distance to the nearest reflecting object.
2. The MCU sends the data over Serial interface to the Bluetooth module. The sensor readings (distance in cm) are formatted in a packet as follows: "sensor1cm,sensor2cm,sensor3cm,sensor4cm" (f.ex. "123,145,23,139"))
3. The Bluetooth module sends the data packet to any other Bluetooth Master (smartphone or a computer)

### To read the payload data on a smartphone, a Bluetooth Serial Terminal app needs to be downloaded (f.ex. "BlueTerm 2" on an Android device, or something similar on iOS).

1. Turn on the prototype by flicking the switch. Pair the bluetooth device with the Bluetooth module on the prototype (called HC-05). The passcode is "1234".
2. Open the bluetooth serial terminal app and connect to the HC-05 device.
3. It should output a stream of unformatted payload data, such as, for example:

12,13,13,10

14,15,16,12

10,10,10,10

### To read the payload data on a computer, a Python script is used. It graphically outputs the payload data on your computer screen using Tkinter. The values are coloured depending on the distance, which is easy to understand once seen. To run the script, one must do the following:
1. Install Python 2.7
2. Install the following packages, if not present: "Tkinter", "pyserial" by using pip or other python package manager. (f.ex. on a *nix macine: "sudo pip install pyserial")
3. Pair the bluetooth module (HC-05) with your computer and connect to it (don't forget to turn the bluetooth on or plug in a bluetooth dongle) (passcode "1234"). Check the serial port number of the module and edit the "present.py" file, line 8, the part which sets the port (currently: "port='COM19'") accordingly (on MS Windows use the Device Manager to find the serial port to which the module is connected).
4. Once that is done, run the Pyhton script by executing into the console "python present.py"
5. A graphical user interface with the sensor values should show up.

### This repository also has the Arduino code and the mechanical CAD (Solidworks) design.

---
If any questions are to arise, do not hesitate to contact us at:

* kristaps.dreija@gmail.com

* shb@ut.ee
